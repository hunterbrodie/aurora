# Aurora

Aurora is a minimal AUR helper written in Rust.

## Features
| Features       | Implemented |
| -----------    | ----------- |
| Install        | Yes         |
| Update         | Yes         |
| Auth Method    | Yes         |
| Edit PKGBUILDS | No          |