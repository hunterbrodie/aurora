use std::fs;
use std::process::Command;

use select::document::Document;
use select::predicate::{Class, Name, Predicate};

use git2::Repository;
use std::path::Path;

use clap::{Arg, App};

mod aur_install;
mod config;


fn main()
{
	let matches = App::new("aurora")
		.version("0.1")
		.author("Hunter Brodie <hunterbrodie@gmail.com>")
		.about("Minimal AUR helper written in Rust")
		.arg(Arg::with_name("install")
				.short("S")
				.long("install")
				.value_name("PACKAGE")
				.help("Install a package from the AUR")
				.takes_value(true))
		.arg(Arg::with_name("auth")
				.short("a")
				.long("auth")
				.value_name("AUTH")
				.help("Change auth method")
				.takes_value(true))
		.arg(Arg::with_name("update")
				.short("u")
				.long("update")
				.help("Update AUR packages"))
		.arg(Arg::with_name("sync")
				.short("y")
				.long("sync")
				.help("Returns packages that are out of date"))
		.get_matches();

	
	if matches.is_present("auth")
	{
		let auth = matches.value_of("auth").unwrap();

		config::update_config_auth(auth.to_string());
	}
	
	if matches.is_present("update")
	{
		update();
	}
	else if matches.is_present("install")
	{
		let package = matches.value_of("install").unwrap();
	
		let search_results = search_aur(package.to_string()).unwrap();
		
		for i in 0..search_results.len()
		{
			println!("[{}] {}", i + 1, &search_results[i]);
		}
		
		let mut input_text = String::new();
				std::io::stdin()
					.read_line(&mut input_text)
					.expect("failed to read from stdin");
			
		let trimmed = input_text.trim();
	
		match trimmed.parse::<usize>()
		{
			Ok(i) => 
			{
				config::update_config_packages(aur_install::install(search_results[i - 1].clone(), config::get_auth()))
			},
			Err(_) => println!("failed to read input"),
		};
	}
	else if matches.is_present("sync")
	{
		let packages = sync();
		for p in packages
		{
			println!("{}", p.0);
		}
	}
}

fn search_aur(term: String) -> Result<Vec<String>, Box<dyn std::error::Error>>
{
	let url = format!("https://aur.archlinux.org/packages/?O=0&SeB=nd&K={}&outdated=&SB=p&SO=d&PP=50&do_Search=Go", term);

	let resp = reqwest::blocking::get(&url)?.text()?;

	let results_page = Document::from(resp.as_str());
	let mut results: Vec<String> = Vec::new();
	
	for node in results_page.find(Class("results").descendant(Name("tbody")).descendant(Name("a")))
	{
		if !String::from(node.attr("href").unwrap()).contains("?K=")
		{
			results.push(node.text().clone());
		}
	}

	Ok(results)
}

fn update()
{
	let packages = sync();

	Command::new(config::get_auth())
		.arg("pacman")
		.arg("-Syu")
		.output()
		.expect("failed to update");
	for p in packages
	{
		config::update_config_packages(aur_install::install(p.0, config::get_auth()))
	}
}

fn sync() -> Vec<(String, String)>
{
	// TODO - return a list of repositories so it doesnt clone twice
	let mut packages = config::get_config_packages();
	if packages.len() > 0
	{
		packages.retain(|p|
		{
			let dir = format!("/tmp/aurora/{}", &p.0);
			
			let url = format!("https://aur.archlinux.org/{}.git", &p.0);
			if Path::new(&dir).exists()
			{
				fs::remove_dir_all(&dir).unwrap();
			}
			
			let repo = match Repository::clone(&url, &dir)
			{
				Ok(repo) => repo,
				Err(e) => panic!("failed to clone respository: {}", e),
			};
			let revparse = repo.revparse("HEAD")
				.unwrap()
				.from()
				.unwrap()
				.id()
				.to_string();
			!p.1.eq(&revparse)
		});
	}

	packages
}

