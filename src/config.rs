use std::fs;
use std::fs::File;
use std::io::prelude::*;
use serde::{Serialize, Deserialize};
use xdg;

#[derive(Serialize, Deserialize)]
struct Config
{
	auth: String,
	packages: Vec<(String, String)>,
}

pub fn update_config_packages(versions: Vec<(String, String)>)
{
	let xdg_dirs = xdg::BaseDirectories::with_prefix("aurora").unwrap();
	let config_file_path = xdg_dirs.place_config_file("config.toml").unwrap();

	if config_file_path.exists()
	{
		let mut conf_file = File::open(&config_file_path).unwrap();
		let mut conf_string = String::new();
		conf_file.read_to_string(&mut conf_string).unwrap();

		let mut conf_file = fs::OpenOptions::new().write(true).truncate(true).open(&config_file_path).unwrap();
		
		let mut conf_struct: Config = toml::from_str(&conf_string).unwrap();
		conf_struct.packages.extend(versions);
		conf_string = toml::to_string(&conf_struct).unwrap();

		conf_file.write_all(conf_string.as_bytes()).unwrap();
	}
	else
	{
		let mut conf_file = File::create(config_file_path).unwrap();
		let conf_struct = Config
		{
			auth: String::new(),
			packages: versions,
		};
		let conf_string = toml::to_string(&conf_struct).unwrap();
		conf_file.write_all(conf_string.as_bytes()).unwrap();
	}
}

pub fn get_config_packages() -> Vec<(String, String)>
{
	let xdg_dirs = xdg::BaseDirectories::with_prefix("aurora").unwrap();
	let config_file_path = xdg_dirs.place_config_file("config.toml").unwrap();

	let mut packages: Vec<(String, String)> = Vec::new();

	if config_file_path.exists()
	{
		let mut conf_file = File::open(&config_file_path).unwrap();
		let mut conf_string = String::new();
		conf_file.read_to_string(&mut conf_string).unwrap();
		
		let conf_struct: Config = toml::from_str(&conf_string).unwrap();

		packages = conf_struct.packages;
	}

	packages
}

pub fn update_config_auth(auth_method: String)
{
	let xdg_dirs = xdg::BaseDirectories::with_prefix("aurora").unwrap();
	let config_file_path = xdg_dirs.place_config_file("config.toml").unwrap();

	if config_file_path.exists()
	{
		let mut conf_file = File::open(&config_file_path).unwrap();
		let mut conf_string = String::new();
		conf_file.read_to_string(&mut conf_string).unwrap();

		let mut conf_file = fs::OpenOptions::new().write(true).truncate(true).open(&config_file_path).unwrap();
		
		let mut conf_struct: Config = toml::from_str(&conf_string).unwrap();
		conf_struct.auth = auth_method;

		conf_string = toml::to_string(&conf_struct).unwrap();

		conf_file.write_all(conf_string.as_bytes()).unwrap();
	}
	else
	{
		let mut conf_file = File::create(config_file_path).unwrap();
		let conf_struct = Config
		{
			auth: auth_method,
			packages: Vec::new(),
		};
		let conf_string = toml::to_string(&conf_struct).unwrap();
		conf_file.write_all(conf_string.as_bytes()).unwrap();
	}
}

pub fn get_auth() -> String
{
	let xdg_dirs = xdg::BaseDirectories::with_prefix("aurora").unwrap();
	let config_file_path = xdg_dirs.place_config_file("config.toml").unwrap();

	let mut auth = String::new();

	if config_file_path.exists()
	{
		let mut conf_file = File::open(&config_file_path).unwrap();
		let mut conf_string = String::new();
		conf_file.read_to_string(&mut conf_string).unwrap();
		
		let conf_struct: Config = toml::from_str(&conf_string).unwrap();

		auth = conf_struct.auth;
	}

	if !auth.is_empty()
	{
		auth
	}
	else
	{
		panic!("choose authentication method")
	}
}