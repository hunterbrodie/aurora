use std::env;

use std::fs;
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::path::Path;
use std::process::{Command, Stdio};

use git2::Repository;

pub fn install(package: String, auth: String) -> Vec<(String, String)>
{
	let dir = format!("/tmp/aurora/{}", &package);

	let url = format!("https://aur.archlinux.org/{}.git", &package);
	if Path::new(&dir).exists()
	{
		fs::remove_dir_all(&dir).unwrap();
	}

	let repo = match Repository::clone(&url, &dir)
	{
		Ok(repo) => repo,
		Err(e) => panic!("failed to clone respository: {}", e),
	};

	let build_dir = Path::new(&dir);
	env::set_current_dir(build_dir).unwrap();

	let t = parse_pkgbuild(&dir);

	let mut versions: Vec<(String, String)> = Vec::new();
	let ver = repo.revparse("HEAD").unwrap();
	versions.push((package, ver.from().unwrap().id().to_string()));

	for p in t.0
	{
		versions.extend(install(p, auth.clone()));
	}
	for p in &t.1
	{
		versions.extend(install(p.to_string(), auth.clone()));
	}

	env::set_current_dir(build_dir).unwrap();

	Command::new("makepkg")
		.arg("-si")
		.output()
		.expect("failed to execute pacman");

	for p in &t.1
	{
		Command::new(&auth)
			.arg("pacman")
			.arg("-Rns")
			.arg(p)
			.output()
			.expect("failed to remove make dependencies");
	}

	versions
}

fn parse_pkgbuild(path: &String) -> (Vec<String>, Vec<String>, Vec<String>)
{
	let pkgbuild_path = format!("{}/PKGBUILD", path);
	let dir = Path::new(&pkgbuild_path);
	let pkgbuild = File::open(dir).expect("can't read PKGBUILD");
	let buf_reader = BufReader::new(pkgbuild);

	let mut d: Vec<String> = Vec::new();
	let mut m: Vec<String> = Vec::new();
	let mut o: Vec<String> = Vec::new();

	let mut check = 'f';
	for line in buf_reader.lines()
	{
		let line = line.unwrap();

		if line.starts_with("depends")
		{
			check = 'd';
		}
		else if line.starts_with("makedepends")
		{
			check = 'm';
		}
		else if line.starts_with("optdepends")
		{
			check = 'o';
		}

		match check
		{
			'd' => 
			{
				d.extend(trim_dependencies(&line));
			}
			'm' => 
			{
				m.extend(trim_dependencies(&line));
			}
			'o' => 
			{
				o.extend(trim_dependencies(&line));
			}
			_ => ()
		}

		if line.contains(")")
		{
			check = 'f';
		}
	}

	(d, m, o)
}

fn trim_dependencies(line: &str) -> Vec<String>
{

	let mut v = line.split("\'")
		.map(|d| d.trim().to_string())
		.collect::<Vec<String>>();

	if v.len() > 0
	{
		if v[0].contains("depends")
		{
			v.swap_remove(0);
		}
	}

	v.retain(|d| !d.eq(")") && !d.is_empty() && !official_package(d));

	v
}

fn official_package(package: &String) -> bool
{
	let status = Command::new("pacman")
		.arg("-Ss")
		.arg(format!("^{}$", package))
		.stdout(Stdio::null())
		.status()
		.expect("failed to execute pacman");

	status.success()
}